﻿using BusinessObjects.Entities;
using DataAcess;
using Repositories.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly CategoryDAO _categoryDAO;

        public CategoryRepository()
        {
            _categoryDAO = new CategoryDAO();
        }
        public async Task AddCategory(string categoryName) => await _categoryDAO.AddCategory(categoryName);
        public async Task<Category> GetCategoryById(int categoryId) => await _categoryDAO.GetCategoryById(categoryId);
        public async Task<Category> GetCategoryByName(string categoryName) => await _categoryDAO.GetCategoryByName(categoryName);
        public async Task<IEnumerable<Category>> GetCategoryList() => await _categoryDAO.GetCategoryList();
    }
}
