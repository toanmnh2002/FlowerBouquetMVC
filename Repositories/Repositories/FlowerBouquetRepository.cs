﻿using BusinessObjects.Entities;
using DataAcess;
using Repositories.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.Repositories
{
    public class FlowerBouquetRepository : IFlowerBouquetRepository
    {
        private readonly FlowerBouquetDAO _flowerBouquetDAO;
        public FlowerBouquetRepository()
        {
            _flowerBouquetDAO = new FlowerBouquetDAO();
        }
        public async Task Add(FlowerBouquet flowerBouquet) => await _flowerBouquetDAO.Add(flowerBouquet);
        public async Task Update(FlowerBouquet flowerBouquet) => await _flowerBouquetDAO.Update(flowerBouquet);
        public async Task Delete(int id) => await _flowerBouquetDAO.Delete(id);
        public Task<FlowerBouquet> GetFlowerBouquetById(int flowerBouquetId, IEnumerable<FlowerBouquet> searchList = null) => _flowerBouquetDAO.GetFlowerBouquetById(flowerBouquetId, searchList);
        public Task<FlowerBouquet> GetFlowerBouquetByName(string flowerBouquetName, IEnumerable<FlowerBouquet> searchList = null) => _flowerBouquetDAO.GetFlowerBouquetByName(flowerBouquetName, searchList);
        public Task<IEnumerable<FlowerBouquet>> GetList(bool order = false) => _flowerBouquetDAO.GetList(order);
        public Task<int> GetNextId() => _flowerBouquetDAO.GetNextId();
        public Task<IEnumerable<FlowerBouquet>> SearchFlowerBouquetByName(string name, IEnumerable<FlowerBouquet> searchList = null) => _flowerBouquetDAO.SearchFlowerBouquet(name, searchList);
        public Task<IEnumerable<FlowerBouquet>> SearchFlowerBouquetByUnitRange(int startUnit, int endUnit, IEnumerable<FlowerBouquet> searchList = null) => _flowerBouquetDAO.SearchFlowerBouquet(startUnit, endUnit, searchList);
        public Task<IEnumerable<FlowerBouquet>> SearchFlowerBouquetByPriceRange(decimal startPrice, decimal endPrice, IEnumerable<FlowerBouquet> searchList = null) => _flowerBouquetDAO.SearchFlowerBouquet(startPrice, endPrice, searchList);
    }
}
