﻿using BusinessObjects.Entities;
using DataAcess;
using Repositories.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly CustomerDAO _customerDAO;
        public CustomerRepository()
        {
            _customerDAO = new CustomerDAO();
        }
        public async Task AddCustomer(Customer customer) => await _customerDAO.AddCustomer(customer);
        public async Task DeleteCustomer(int customerID) => await _customerDAO.Delete(customerID);
        public async Task UpdateCustomer(Customer customer) => await _customerDAO.UpdateCustomer(customer);
        public async Task<IEnumerable<Customer>> SearchCustomer(string name) => await _customerDAO.SearchCustomerByName(name);
        public async Task<IEnumerable<Customer>> SearchCustomerByCity(string country, string city, IEnumerable<Customer> searchList) => await _customerDAO.SearchCustomerByCity(country, city, searchList);
        public async Task<IEnumerable<Customer>> SearchCustomerByCountry(string country, IEnumerable<Customer> searchList) => await _customerDAO.SearchCustomerByCountry(country, searchList);
        public async Task<IEnumerable<Customer>> GetAll() => await _customerDAO.GetAll();
        public async Task<IEnumerable<Customer>> GetAllCustomer() => await _customerDAO.GetAllCustomer();
        public async Task<Customer> GetByEmail(string email) => await _customerDAO.GetByEmail(email);
        public async Task<Customer> GetCustomerByID(int customerID) => await _customerDAO.GetCustomerByID(customerID);
        public async Task<int> GetNextMemberId() => await _customerDAO.GetNextId();
        public async Task<Customer> Login(string email, string password) => await _customerDAO.Login(email, password);
        public async Task<bool> IsLogin(string email) => await _customerDAO.IsLogin(email);
        public async Task<bool> IsCustomer(string email) => await _customerDAO.IsCustomer(email);
        public async Task<bool> IsAdmin(string email) => await _customerDAO.IsAdmin(email);


    }
}
