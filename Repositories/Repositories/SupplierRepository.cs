﻿using BusinessObjects.Entities;
using DataAcess;
using Repositories.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.Repositories
{
    public class SupplierRepository : ISupplierRepository
    {
        private readonly SupplierDAO _supplierDAO;

        public SupplierRepository()
        {
            _supplierDAO = new SupplierDAO();
        }

        public async Task AddSupplier(string supplierName) => await _supplierDAO.AddSupplier(supplierName);
        public async Task<Supplier> GetSupplierById(int supplierId) => await _supplierDAO.GetCategoryById(supplierId);
        public async Task<Supplier> GetSupplierByName(string supplierName) => await _supplierDAO.GetSupplierByName(supplierName);
        public async Task<IEnumerable<Supplier>> GetSupplierList() => await _supplierDAO.GetSupplierList();

    }
}
