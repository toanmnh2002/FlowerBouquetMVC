﻿using BusinessObjects.Entities;
using DataAcess;
using Repositories.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private readonly OrderDAO _orderDAO;

        public OrderRepository()
        {
            _orderDAO = new OrderDAO();
        }
        public async Task<Order> Add(Order order)=>await _orderDAO.Add(order);
        public async Task DeleteByCustomer(int customerId)=>await _orderDAO.DeleteByCustomer(customerId);
        public async Task DeleteByID(int id)=>await _orderDAO.DeleteByID(id);
        public async Task<IEnumerable<Order>> GetAll()=>await _orderDAO.GetAll();
        public async Task<IEnumerable<Order>> GetByCustomerId(int CustomerId)=>await _orderDAO.GetByCustomerId(CustomerId);
        public async Task<Order> GetById(int id)=>await _orderDAO.GetByOrderId(id);
        public async Task<IEnumerable<Order>> GetOrdersByDateRange(int customerId, DateTime startDate, DateTime endDate) => await _orderDAO.GetOrdersByDateRange(customerId,startDate,endDate);
        public async Task Update(Order order) => await _orderDAO.Update(order);
    }
}
