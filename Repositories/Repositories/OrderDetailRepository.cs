﻿using BusinessObjects.Entities;
using DataAcess;
using Repositories.IRepositories;
namespace Repositories.Repositories
{
    public class OrderDetailRepository : IOrderDetailRepository
    {
        private readonly OrderDetailDAO _orderDetailDAO;

        public OrderDetailRepository()
        {
            _orderDetailDAO = new OrderDetailDAO();
        }
        public async Task Add(OrderDetail orderId) => await _orderDetailDAO.Add(orderId);
        public async Task DeleteByFlowerBouquet(int flowerBouquetId) => await _orderDetailDAO.DeleteByFlowerBouquet(flowerBouquetId);
        public async Task DeleteOrderDetailsByOrderId(int orderId) => await _orderDetailDAO.DeleteOrderDetailsByOrderId(orderId);
        public async Task<IEnumerable<OrderDetail>> GetOrderDetailsByOrderId(int orderId) => await _orderDetailDAO.GetOrderDetailsByOrderId(orderId);
        public async Task<decimal> GetOrderTotal(int orderId) => await _orderDetailDAO.GetOrderTotal(orderId);
        public async Task<IEnumerable<OrderDetail>> GetAllAndDescending()=>await _orderDetailDAO.GetAllAndDescending();
    }
}
