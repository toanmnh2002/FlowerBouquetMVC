﻿using BusinessObjects.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.IRepositories
{
    public interface IFlowerBouquetRepository
    {
        Task<FlowerBouquet> GetFlowerBouquetByName(string flowerBouquetName, IEnumerable<FlowerBouquet> searchList = null);
        Task<FlowerBouquet> GetFlowerBouquetById(int flowerBouquetId, IEnumerable<FlowerBouquet> searchList = null);
        Task<IEnumerable<FlowerBouquet>> GetList(bool order = false);
        Task<int> GetNextId();
        Task Add(FlowerBouquet flowerBouquet);
        Task Update(FlowerBouquet flowerBouquet);
        Task Delete(int id);
        Task<IEnumerable<FlowerBouquet>> SearchFlowerBouquetByName(string name, IEnumerable<FlowerBouquet> searchList = null);
        Task<IEnumerable<FlowerBouquet>> SearchFlowerBouquetByUnitRange(int startUnit, int endUnit, IEnumerable<FlowerBouquet> searchList = null);
        Task<IEnumerable<FlowerBouquet>> SearchFlowerBouquetByPriceRange(decimal startPrice, decimal endPrice, IEnumerable<FlowerBouquet> searchList = null);
    }
}
