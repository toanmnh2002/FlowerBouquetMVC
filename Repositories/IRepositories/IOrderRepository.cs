﻿using BusinessObjects.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.IRepositories
{
    public interface IOrderRepository
    {
        Task<IEnumerable<Order>> GetByCustomerId(int CustomerId);
        Task<Order> GetById(int id);
        Task<IEnumerable<Order>> GetOrdersByDateRange(int memberId, DateTime startDate, DateTime endDate);
        Task<IEnumerable<Order>> GetAll();
        Task<Order> Add(Order order);
        Task Update(Order order);
        Task DeleteByID(int id);
        Task DeleteByCustomer(int customerId);
    }
}
