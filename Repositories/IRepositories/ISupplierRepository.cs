﻿using BusinessObjects.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.IRepositories
{
    public interface ISupplierRepository
    {
        Task<IEnumerable<Supplier>> GetSupplierList();
        Task<Supplier> GetSupplierById(int supplierId);
        Task<Supplier> GetSupplierByName(string supplierName);
        Task AddSupplier(string supplierName);
    }
}
