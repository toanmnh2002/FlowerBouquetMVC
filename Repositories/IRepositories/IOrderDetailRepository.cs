﻿using BusinessObjects.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.IRepositories
{
    public interface IOrderDetailRepository
    {
        Task<IEnumerable<OrderDetail>> GetOrderDetailsByOrderId(int orderId);
        Task<IEnumerable<OrderDetail>> GetAllAndDescending();
        Task<decimal> GetOrderTotal(int orderId);
        Task Add(OrderDetail orderId);
        Task DeleteOrderDetailsByOrderId(int orderId);
        Task DeleteByFlowerBouquet(int FlowerBouquetId);
    }
}
