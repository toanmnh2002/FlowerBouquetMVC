﻿using BusinessObjects.Entities;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects.FluentValidation
{
    public class CustomerValidation : AbstractValidator<Customer>
    {
        public CustomerValidation()
        {
            RuleFor(x => x.CustomerId)
               .NotEmpty().WithMessage("CustomerId ID is required.")
               .GreaterThan(0).WithMessage("CustomerId ID must be greater than 0.");

            RuleFor(x => x.Email)
                .NotEmpty()
                .WithMessage("Email is required.")
                .EmailAddress()
                .WithMessage("Invalid email address.")
                .MaximumLength(100)
                .WithMessage("Email at most 100 characters ");

            RuleFor(x => x.CustomerName)
                .NotEmpty()
                .WithMessage("Customer name is required.")
                .MaximumLength(180)
                .WithMessage("Customer name at most 180 characters ");

            RuleFor(x => x.City)
                .NotEmpty()
                .WithMessage("City is required.")
                .MaximumLength(15)
                .WithMessage("City at most 15 characters "); ;

            RuleFor(x => x.Country)
                .NotEmpty()
                .WithMessage("Country is required.")
                .MaximumLength(15)
                .WithMessage("Country at most 100 characters "); ;

            RuleFor(x => x.Password)
                .NotEmpty()
                .WithMessage("Password is required.")
                .Matches(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,30}$")
                .WithMessage("Password must be at least 8 characters, at most 30 characters long and contain at least one uppercase letter, one lowercase letter, one digit, and one special character.");

            RuleFor(x => x.Birthday)
                .NotNull()
                .WithMessage("Birthday is required.")
                .Must(BeValidBirthday)
                .WithMessage("Invalid birthday.");
        }
        private bool BeValidBirthday(DateTime? birthday)
        {
            return birthday.HasValue && birthday.Value < DateTime.Now;
        }
    }
}