﻿using BusinessObjects.Entities;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects.FluentValidation
{
    public class FlowerBouquetValidation : AbstractValidator<FlowerBouquet>
    {
        public FlowerBouquetValidation()
        {
            RuleFor(x => x.FlowerBouquetId)
                    .NotEmpty().WithMessage("FlowerBouquetId ID is required.")
                    .GreaterThan(0).WithMessage("FlowerBouquetId ID must be greater than 0.");
            RuleFor(x => x.CategoryId)
          .NotEmpty().WithMessage("CategoryId is required.")
          .GreaterThan(0).WithMessage("CategoryId ID must be greater than 0.");
            RuleFor(x => x.SupplierId)
            .NotEmpty().WithMessage("Supplier ID is required.")
            .GreaterThan(0).WithMessage("Supplier ID must be greater than 0.");
            RuleFor(x => x.CategoryId)
                .NotEmpty().WithMessage("Category ID is required.")
                .GreaterThan(0).WithMessage("Category ID must be greater than 0.");
            RuleFor(x => x.FlowerBouquetName)
            .NotEmpty().WithMessage("Flower bouquet name is required.")
            .MaximumLength(40).WithMessage("Flower bouquet name cannot exceed 40 characters.");
            RuleFor(x => x.Description)
                .NotEmpty().WithMessage("Description is required.")
                .MaximumLength(220).WithMessage("Description cannot exceed 220 characters.");
            RuleFor(x => x.UnitPrice)
                .NotEmpty().WithMessage("Unit price is required.")
                .GreaterThan(0).WithMessage("Unit price must be greater than 0.");
            RuleFor(x => x.UnitsInStock)
                .NotEmpty().WithMessage("Units in stock is required.");
        }
    }
}
