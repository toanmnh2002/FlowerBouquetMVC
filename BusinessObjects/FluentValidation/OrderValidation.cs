﻿using BusinessObjects.Entities;
using FluentValidation;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects.FluentValidation
{
    public class OrderValidation : AbstractValidator<Order>
    {
        public OrderValidation()
        {
            RuleFor(x => x.OrderDate)
            .NotEmpty()
            .Must((order, orderDate) => orderDate < order.ShippedDate)
            .WithMessage("Order Date must be earlier than Shipped Date");

            RuleFor(x => x.ShippedDate)
                .Must((order, shippedDate) => shippedDate > order.OrderDate)
                .WithMessage("Shipped Date must be later than Order Date")
                .When(x => x.ShippedDate.HasValue);
            RuleFor(x => x.CustomerId)
            .GreaterThan(0).WithMessage("Invalid CustomerId");
            RuleFor(x => x.Total)
                .NotNull()
                .GreaterThanOrEqualTo(0).WithMessage("Invalid Total");
            ;
            RuleFor(x => x.OrderStatus)
                .NotEmpty()
                .MaximumLength(100).WithMessage("Invalid Status");
        }
    }
}
