﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Repositories.IRepositories;

namespace NguyenManhToan_SE1604_A02.Controllers
{
    public class LogoutController:BaseController
    {
        public LogoutController(ICustomerRepository customerRepository, IOrderRepository orderRepository, IOrderDetailRepository orderDetailRepository, IFlowerBouquetRepository flowerBouquetRepository, ICategoryRepository categoryRepository, ISupplierRepository supplierRepository) : base(customerRepository, orderRepository, orderDetailRepository, flowerBouquetRepository, categoryRepository, supplierRepository)
        {
        }

        public IActionResult Index()
        {
            HttpContext.SignOutAsync();
            return RedirectToAction("Index", "User");
        }
    }
}
