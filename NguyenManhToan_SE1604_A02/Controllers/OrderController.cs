﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BusinessObjects.Entities;
using Repositories.IRepositories;
using FluentValidation;
using FluentValidation.Results;
using FluentValidation.AspNetCore;

namespace NguyenManhToan_SE1604_A02.Controllers
{
    public class OrderController : BaseController
    {
        private readonly IValidator<Order> _validation;

        public OrderController(ICustomerRepository customerRepository, IOrderRepository orderRepository, IOrderDetailRepository orderDetailRepository, IFlowerBouquetRepository flowerBouquetRepository, ICategoryRepository categoryRepository, ISupplierRepository supplierRepository, IValidator<Order> validation) : base(customerRepository, orderRepository, orderDetailRepository, flowerBouquetRepository, categoryRepository, supplierRepository)
        {
            _validation = validation;
        }
        // GET: Order
        public async Task<IActionResult> Index()
        {
            string userLogin = HttpContext.Session.GetString("USERLOGIN");
            if (!(await _customerRepository.IsLogin(userLogin)))
            {
                return RedirectToAction("Index", "User");
            }
            var orders = await _orderRepository.GetAll();
            return orders is not null ?
                        View(orders.ToList()) :
                        Problem("Entity set 'FuflowerBouquetManagementContext.Orders'  is null.");
        }

        // GET: Order/Details/5
        public async Task<IActionResult> Details(int id)
        {
            try
            {
                string userLogin = HttpContext.Session.GetString("USERLOGIN");
                if (!(await _customerRepository.IsAdmin(userLogin)))
                {
                    return RedirectToAction("Index", "User");
                }
                if (await _orderRepository.GetById(id) is null)
                {
                    throw new Exception("Order ID is not found!!!");
                }
                var order = await _orderRepository.GetById(id);
                if (order is null)
                {
                    throw new Exception("Order ID is not found!!!");
                }
                return View(order);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View();
            }
        }

        // GET: Order/Create
        public async Task<IActionResult> Create()
        {
            try
            {
                string userLogin = HttpContext.Session.GetString("USERLOGIN");
                if (!(await _customerRepository.IsAdmin(userLogin)))
                {
                    return RedirectToAction("Index", "User");
                }
                var customer = await _customerRepository.GetAllCustomer();
                var customerList = new SelectList(customer.ToDictionary(x => x.CustomerId, x => x.CustomerName), "Key", "Value");
                ViewBag.Customer = customerList;
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View();
            }
            return View();
        }

        // POST: Order/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("OrderId,CustomerId,OrderDate,ShippedDate,Total,OrderStatus")] Order order)
        {
            try
            {
                //if (ModelState.IsValid)
                //{
                ValidationResult rs = await _validation.ValidateAsync(order);
                if (rs.IsValid)
                {
                    await _orderRepository.Add(order);
                    return RedirectToAction(nameof(Index));
                }
                rs.AddToModelState(this.ModelState);
                return View("Create", order);
                //}
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View("Create");
            }
            //return null;
        }

        // GET: Order/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            try
            {
                string userLogin = HttpContext.Session.GetString("USERLOGIN");
                if (!(await _customerRepository.IsAdmin(userLogin)))
                {
                    return RedirectToAction("Index", "User");
                }
                var order = await _orderRepository.GetById(id);
                if (order is null)
                {
                    throw new Exception("Order ID is not found!!!");
                }
                var customer = await _customerRepository.GetAllCustomer();
                var customerList = new SelectList(customer.ToDictionary(x => x.CustomerId, x => x.CustomerName), "Key", "Value");
                ViewBag.Customer = customerList;
                return View(order);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View();
            }
        }

        // POST: Order/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("OrderId,CustomerId,OrderDate,ShippedDate,Total,OrderStatus")] Order order)
        {
            return View();
        }

        // GET: Order/Delete/5
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                string userLogin = HttpContext.Session.GetString("USERLOGIN");
                if (!(await _customerRepository.IsAdmin(userLogin)))
                {
                    return RedirectToAction("Index", "User");
                }
                if (await _orderRepository.GetById(id) is null)
                {
                    return NotFound();
                }
                var order = await _orderRepository.GetById(id);
                if (order is null)
                {
                    throw new Exception("Order ID is not found!!!");
                }
                return View(order);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View();
            }
        }

        // POST: Order/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (await _orderRepository.GetById(id) is null)
            {
                return Problem("Entity set 'FuflowerBouquetManagementContext.Orders'  is null.");
            }
            var order = await _orderRepository.GetById(id);
            if (order is not null)
            {
                await _orderRepository.DeleteByID(order.OrderId);
            }
            return RedirectToAction(nameof(Index));
        }
    }
}
