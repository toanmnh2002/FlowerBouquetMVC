﻿using Microsoft.AspNetCore.Mvc;
using NguyenManhToan_SE1604_A02.Models;
using Repositories.IRepositories;
using System.Diagnostics;

namespace NguyenManhToan_SE1604_A02.Controllers
{
    public class BaseController : Controller
    {
        protected readonly ICustomerRepository _customerRepository;
        protected readonly IOrderRepository _orderRepository;
        protected readonly IOrderDetailRepository _orderDetailRepository;
        protected readonly IFlowerBouquetRepository _flowerBouquetRepository;
        protected readonly ICategoryRepository _categoryRepository;
        protected readonly ISupplierRepository _supplierRepository;


        public BaseController(ICustomerRepository customerRepository, IOrderRepository orderRepository, IOrderDetailRepository orderDetailRepository, IFlowerBouquetRepository flowerBouquetRepository, ICategoryRepository categoryRepository, ISupplierRepository supplierRepository)
        {
            _customerRepository = customerRepository;
            _orderRepository = orderRepository;
            _orderDetailRepository = orderDetailRepository;
            _flowerBouquetRepository = flowerBouquetRepository;
            _categoryRepository = categoryRepository;
            _supplierRepository = supplierRepository;
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
