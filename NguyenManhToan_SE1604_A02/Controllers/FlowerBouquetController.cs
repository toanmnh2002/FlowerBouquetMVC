﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BusinessObjects.Entities;
using Repositories.IRepositories;
using FluentValidation;
using Repositories.Repositories;
using FluentValidation.Results;
using FluentValidation.AspNetCore;

namespace NguyenManhToan_SE1604_A02.Controllers
{
    public class FlowerBouquetController : BaseController
    {
        private readonly IValidator<FlowerBouquet> _validation;

        public FlowerBouquetController(ICustomerRepository customerRepository, IOrderRepository orderRepository, IOrderDetailRepository orderDetailRepository, IFlowerBouquetRepository flowerBouquetRepository, ICategoryRepository categoryRepository, ISupplierRepository supplierRepository, IValidator<FlowerBouquet> validation) : base(customerRepository, orderRepository, orderDetailRepository, flowerBouquetRepository, categoryRepository, supplierRepository)
        {
            _validation = validation;
        }


        // GET: FlowerBouquet
        public async Task<IActionResult> Index()
        {
            string userLogin = HttpContext.Session.GetString("USERLOGIN");
            if (!(await _customerRepository.IsAdmin(userLogin)))
            {
                return RedirectToAction("Index", "User");
            }
            var flowerBouquets = await _flowerBouquetRepository.GetList();
            return flowerBouquets is not null ? View(flowerBouquets.ToList()) :
                Problem("Entity set 'FuflowerBouquetManagementContext.FlowerBouquets'  is null.");
        }

        // GET: FlowerBouquet/Details/5
        public async Task<IActionResult> Details(int id)
        {
            try
            {
                string userLogin = HttpContext.Session.GetString("USERLOGIN");
                if (!(await _customerRepository.IsAdmin(userLogin)))
                {
                    return RedirectToAction("Index", "User");
                }
                if (await _flowerBouquetRepository.GetFlowerBouquetById(id) is null)
                {
                    throw new Exception("FlowerBouquet ID is not found!!!");
                }
                var flowerBouquet = await _flowerBouquetRepository.GetFlowerBouquetById(id);
                if (flowerBouquet is null)
                {
                    throw new Exception("FlowerBouquet ID is not found!!!");
                }
                return View(flowerBouquet);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View();
            }
        }

        // GET: FlowerBouquet/Create
        public async Task<IActionResult> Create()
        {
            try
            {
                string userLogin = HttpContext.Session.GetString("USERLOGIN");
                if (!(await _customerRepository.IsAdmin(userLogin)))
                {
                    return RedirectToAction("Index", "User");
                }
                var categories = await _categoryRepository.GetCategoryList();
                var suppliers = await _supplierRepository.GetSupplierList();
                var categoriesList = new SelectList(categories.ToDictionary(cate => cate.CategoryId, cate => cate.CategoryName), "Key", "Value");
                var supplierList = new SelectList(suppliers.ToDictionary(sup => sup.SupplierId, sup => sup.SupplierName), "Key", "Value");

                ViewBag.Category = categoriesList;
                ViewBag.Supplier = supplierList;
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View();
            }
            return View();
        }

        // POST: FlowerBouquet/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("FlowerBouquetId,CategoryId,FlowerBouquetName,Description,UnitPrice,UnitsInStock,FlowerBouquetStatus,SupplierId")] FlowerBouquet flowerBouquet)
        {
            try
            {
                //if (ModelState.IsValid)
                //{
                ValidationResult rs = await _validation.ValidateAsync(flowerBouquet);
                if (rs.IsValid)
                {
                    await _flowerBouquetRepository.Add(flowerBouquet);
                    return RedirectToAction(nameof(Index));
                }
                rs.AddToModelState(this.ModelState);
                return View("Create", flowerBouquet);
                //}
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View("Create");
            }
            //return null;
        }

        // GET: FlowerBouquet/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            try
            {
                string userLogin = HttpContext.Session.GetString("USERLOGIN");
                if (!(await _customerRepository.IsAdmin(userLogin)))
                {
                    return RedirectToAction("Index", "User");
                }
                FlowerBouquet flowerBouquet = await _flowerBouquetRepository.GetFlowerBouquetById(id);
                if (flowerBouquet is null)
                {
                    throw new Exception("FlowerBouquet ID is not found!!!");
                }
                var categories = await _categoryRepository.GetCategoryList();
                var suppliers = await _supplierRepository.GetSupplierList();
                var categoriesList = new SelectList(categories.ToDictionary(cate => cate.CategoryId, cate => cate.CategoryName), "Key", "Value");
                var supplierList = new SelectList(suppliers.ToDictionary(sup => sup.SupplierId, sup => sup.SupplierName), "Key", "Value");
                ViewBag.Category = categoriesList;
                ViewBag.Supplier = supplierList;
                return View(flowerBouquet);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View();
            }
        }

        // POST: FlowerBouquet/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("FlowerBouquetId,CategoryId,FlowerBouquetName,Description,UnitPrice,UnitsInStock,FlowerBouquetStatus,SupplierId")] FlowerBouquet flowerBouquet)
        {
            try
            {
                ValidationResult rs = await _validation.ValidateAsync(flowerBouquet);
                if (rs.IsValid)
                {
                    await _flowerBouquetRepository.Update(flowerBouquet);
                    return RedirectToAction(nameof(Index));
                }
                rs.AddToModelState(this.ModelState);
                return View("Edit", flowerBouquet);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View("Edit");
            }
        }

        // GET: FlowerBouquet/Delete/5
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                string userLogin = HttpContext.Session.GetString("USERLOGIN");
                if (!(await _customerRepository.IsAdmin(userLogin)))
                {
                    return RedirectToAction("Index", "User");
                }
                if (await _flowerBouquetRepository.GetFlowerBouquetById(id) is null)
                {
                    return NotFound();
                }
                var flowerBouquet = await _flowerBouquetRepository.GetFlowerBouquetById(id);
                if (flowerBouquet is null)
                {
                    throw new Exception("FlowerBouquet ID is not found!!!");
                }
                return View(flowerBouquet);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View();
            }
        }

        // POST: FlowerBouquet/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (await _flowerBouquetRepository.GetFlowerBouquetById(id) is null)
            {
                return Problem("Entity set 'FuflowerBouquetManagementContext.FlowerBouquets'  is null.");
            }
            var flowerBouquet = await _flowerBouquetRepository.GetFlowerBouquetById(id);
            if (flowerBouquet is not null)
            {
                await _flowerBouquetRepository.Delete(flowerBouquet.FlowerBouquetId);
            }
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Search(string search)
        {
            var searchList = await _flowerBouquetRepository.GetList();
            if (!string.IsNullOrEmpty(search))
            {
                searchList = await _flowerBouquetRepository.SearchFlowerBouquetByName(search);
            }
            return View(searchList);
        }
    }
}
