﻿using Microsoft.AspNetCore.Mvc;
using NguyenManhToan_SE1604_A02.Models;
using Repositories.IRepositories;
using System.Diagnostics;

namespace NguyenManhToan_SE1604_A02.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController(ICustomerRepository customerRepository, IOrderRepository orderRepository, IOrderDetailRepository orderDetailRepository, IFlowerBouquetRepository flowerBouquetRepository, ICategoryRepository categoryRepository, ISupplierRepository supplierRepository) : base(customerRepository, orderRepository, orderDetailRepository, flowerBouquetRepository, categoryRepository, supplierRepository)
        {
        }

        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Privacy()
        {
            return View();
        }
        public IActionResult Register()
        {
            return View();
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}