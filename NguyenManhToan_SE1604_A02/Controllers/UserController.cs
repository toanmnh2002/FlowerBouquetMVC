﻿using BusinessObjects.Entities;
using Microsoft.AspNetCore.Mvc;
using Repositories.IRepositories;

namespace NguyenManhToan_SE1604_A02.Controllers
{
    public class UserController : BaseController
    {
        public UserController(ICustomerRepository customerRepository, IOrderRepository orderRepository, IOrderDetailRepository orderDetailRepository, IFlowerBouquetRepository flowerBouquetRepository, ICategoryRepository categoryRepository, ISupplierRepository supplierRepository) : base(customerRepository, orderRepository, orderDetailRepository, flowerBouquetRepository, categoryRepository, supplierRepository)
        {
        }

        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Login(string email, string password)
        {
            try
            {
                Customer result = await _customerRepository.Login(email, password);
                
                if (result.CustomerName is "Admin")
                {
                    HttpContext.Session.SetString("USERLOGIN", result.Email);
                    return RedirectToAction("Index","Admin");
                }
                else
                {
                    HttpContext.Session.SetString("USERLOGIN", result.Email);
                    HttpContext.Session.SetString("CustomerId", result.CustomerId.ToString());
                    return RedirectToAction("ViewProfile", "Customer");
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View("Index");
            }

        }
    }
}

