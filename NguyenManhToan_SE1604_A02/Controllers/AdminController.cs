﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Repositories.IRepositories;

namespace NguyenManhToan_SE1604_A02.Controllers
{
    public class AdminController : BaseController
    {
        public AdminController(ICustomerRepository customerRepository, IOrderRepository orderRepository, IOrderDetailRepository orderDetailRepository, IFlowerBouquetRepository flowerBouquetRepository, ICategoryRepository categoryRepository, ISupplierRepository supplierRepository) : base(customerRepository, orderRepository, orderDetailRepository, flowerBouquetRepository, categoryRepository, supplierRepository)
        {
        }

        public async Task<IActionResult> Index()
        {
            try
            {
                string userLogin = HttpContext.Session.GetString("USERLOGIN");
                if (!(await _customerRepository.IsAdmin(userLogin)))
                {
                    return RedirectToAction("Index", "User");
                    throw new Exception("Access is denied!");
                }
                if (userLogin is null)
                {
                    throw new Exception("User is not login");
                }
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View();
            }
        }

    }
}
