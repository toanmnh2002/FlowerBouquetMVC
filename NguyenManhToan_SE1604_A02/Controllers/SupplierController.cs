﻿using Microsoft.AspNetCore.Mvc;
using Repositories.IRepositories;

namespace NguyenManhToan_SE1604_A02.Controllers
{
    public class SupplierController : BaseController
    {
        public SupplierController(ICustomerRepository customerRepository, IOrderRepository orderRepository, IOrderDetailRepository orderDetailRepository, IFlowerBouquetRepository flowerBouquetRepository, ICategoryRepository categoryRepository, ISupplierRepository supplierRepository) : base(customerRepository, orderRepository, orderDetailRepository, flowerBouquetRepository, categoryRepository, supplierRepository)
        {
        }

        public async Task<IActionResult> Create(string name)
        {
            try
            {
                if (!string.IsNullOrEmpty(name))
                {
                    await _supplierRepository.AddSupplier(name);
                    return Json("Create Supplier successfully!!");
                }
                else
                {
                    throw new Exception("The Supplier Name is empty!!");
                }
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
    }
}
