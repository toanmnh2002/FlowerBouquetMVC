﻿using BusinessObjects.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcess
{
    public class OrderDetailDAO
    {
        private readonly FuflowerBouquetManagementContext _dbContext;

        public OrderDetailDAO()
        {
            _dbContext = new FuflowerBouquetManagementContext();
        }
        public async Task<IEnumerable<OrderDetail>> GetAllAndDescending()
        {
            var result = await _dbContext.OrderDetails.Include(c => c.FlowerBouquet).Include(c => c.Order).OrderByDescending(c => c.UnitPrice).ToListAsync();
            return result;
        }

        public async Task<IEnumerable<OrderDetail>> GetOrderDetailsByOrderId(int orderId)
        {
            IEnumerable<OrderDetail> orderDetails = null;
            try
            {
                orderDetails = await _dbContext.OrderDetails.Where(od => od.OrderId == orderId)
                    .Include(od => od.FlowerBouquet).ToListAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return orderDetails;
        }
        public async Task<decimal> GetOrderTotal(int orderId)
        {
            decimal orderTotal = 0;

            try
            {
                IEnumerable<OrderDetail> orderDetails = await _dbContext.OrderDetails.Where(od => od.OrderId == orderId).ToListAsync();
                foreach (var orderDetail in orderDetails)
                {
                    orderTotal += orderDetail.UnitPrice * (1 - Convert.ToDecimal(orderDetail.Discount));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return Math.Round(orderTotal, 2);
        }

        public async Task Add(OrderDetail carRental)
        {
            try
            {
                await _dbContext.AddAsync(carRental);
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        public async Task DeleteOrderDetailsByOrderId(int orderId)
        {
            try
            {
                IEnumerable<OrderDetail> orderDetails = _dbContext.OrderDetails.Where(od => od.OrderId == orderId);
                _dbContext.RemoveRange(orderDetails);
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task DeleteByFlowerBouquet(int FlowerBouquetId)
        {
            try
            {
                IEnumerable<OrderDetail> orderDetails = _dbContext.OrderDetails.Where(od => od.FlowerBouquetId == FlowerBouquetId);

                _dbContext.RemoveRange(orderDetails);
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}