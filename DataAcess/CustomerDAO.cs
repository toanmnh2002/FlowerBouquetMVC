﻿using BusinessObjects.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualBasic;
using System.Diagnostics.Metrics;
using System.Security.Cryptography.X509Certificates;
using System.Text.Json;

namespace DataAcess
{
    public class CustomerDAO
    {
        private readonly FuflowerBouquetManagementContext _context;

        public CustomerDAO()
        {
            _context = new FuflowerBouquetManagementContext();
        }

        private async Task<Customer> GetDefaultCustomer()
        {
            Customer Default = null;
            using (StreamReader r = new StreamReader("appsettings.json"))
            {
                string json = await r.ReadToEndAsync();
                IConfiguration config = new ConfigurationBuilder()
                                        .SetBasePath(Directory.GetCurrentDirectory())
                                        .AddJsonFile("appsettings.json", true, true)
                                        .Build();
                string email = config["account:defaultAccount:email"];
                string password = config["account:defaultAccount:password"];
                Default = new Customer
                {
                    CustomerId = 0,
                    Email = email,
                    CustomerName = "Admin",
                    City = "",
                    Country = "",
                    Birthday = null,
                    Password = password
                };
            }
            return Default;
        }
        public async Task<Customer> GetByEmail(string email)
        {
            try
            {
                var result = await _context.Customers.FirstOrDefaultAsync(x => x.Email.Equals(email));
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<int> GetNextId()
        {
            int nextId = -1;
            try
            {
                nextId = await _context.Customers.MaxAsync(x => x.CustomerId) + 1;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return nextId;
        }
        public async Task<Customer> GetCustomerByID(int customerID)
        {
            try
            {
                var result = await _context.Customers.FirstOrDefaultAsync(x => x.CustomerId == customerID);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task AddCustomer(Customer customer)
        {
            if (customer is null)
            {
                throw new Exception("Customer is undefined!!");
            }
            try
            {
                if (await GetCustomerByID(customer.CustomerId) is not null || await GetByEmail(customer.Email) is not null)
                {
                    throw new Exception("Customer is existed! Please try again!");
                }
                await _context.Customers.AddAsync(customer);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<IEnumerable<Customer>> GetAll()
        {
            IEnumerable<Customer> customers = null;
            try
            {
                customers = await _context.Customers.ToListAsync();
                Customer admin = await GetDefaultCustomer();
                customers = customers.Append(admin);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return customers;
        }
        public async Task<IEnumerable<Customer>> GetAllCustomer()
        {
            IEnumerable<Customer> customers = null;
            try
            {
                customers = await _context.Customers.ToListAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return customers;
        }

        public async Task UpdateCustomer(Customer customer)
        {
            if (customer is null)
            {
                throw new Exception("Customer is undefined!!");
            }
            try
            {
                Customer cus = await GetCustomerByID(customer.CustomerId);
                if (cus is not null)
                {
                    _context.Entry(cus).State = EntityState.Detached; // Detach the existing entity
                    _context.Entry(customer).State = EntityState.Modified; // Attach and mark as modified
                    //_context.Customers.Update(customer);
                    await _context.SaveChangesAsync();
                }
                else
                {
                    throw new Exception("Customer does not exist!!");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task Delete(int customerId)
        {
            try
            {
                Customer customer = await GetCustomerByID(customerId);
                if (customer != null)
                {
                    _context.Customers.Remove(customer);
                    await _context.SaveChangesAsync();
                }
                else
                {
                    throw new Exception("Customer does not exist!!");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<IEnumerable<Customer>> SearchCustomerByName(string name)
        {
            try
            {
                var result = await _context.Customers.Where(x => x.CustomerName.ToLower().Contains(name.ToLower())).ToListAsync();
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<IEnumerable<Customer>> SearchCustomerByCountry(string country, IEnumerable<Customer> searchList = null)
        {
            IEnumerable<Customer> searchResult = null;
            if (searchList != null)
            {
                if (country is "All")
                {
                    searchResult = searchList.ToList();
                }
                else
                {
                    searchResult = searchList.Where(x => x.Country == country);
                }
            }
            else
            {
                searchResult = await _context.Customers.Where(x => x.Country == country).ToListAsync();
            }
            return searchResult;
        }

        public async Task<IEnumerable<Customer>> SearchCustomerByCity(string country, string city, IEnumerable<Customer> searchList)
        {
            IEnumerable<Customer> searchResult = null;
            var customerSearch = searchList.Where(customer => customer.City == city);

            if (city is "All")
            {
                if (country is "All")
                {
                    searchResult = searchList;
                }
                else
                {
                    customerSearch = searchList.Where(customer => customer.Country == country);
                }
            }
            searchResult = customerSearch;
            return searchResult;
        }
        public async Task<Customer> Login(string email, string password)
        {
            Customer customer = null;
            try
            {
                IEnumerable<Customer> customers = await GetAll();
                customer = customers.SingleOrDefault(x => x.Email.Equals(email) && x.Password.Equals(password));
                if (customer is null)
                {
                    throw new Exception("Login failed! Please check your email and password!!");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return customer;
        }
        public async Task<bool> IsLogin(string email)
        {

            //if (email is null)
            //{
            //    return false;
            //}
            //var listCustomer = await GetAllCustomer();
            //var customerEmails = listCustomer.Select(x => x.Email).ToList();
            //Customer adminLogin = await GetDefaultCustomer();
            //string admin = adminLogin.Email;
            //if (email != admin || customerEmails.Contains(email))
            //{
            //    return false;
            //}
            //return true;
            if (email is null)
            {
                return false;
            }
            var listAllCustomer = await GetAll();
            var customerEmails = listAllCustomer.Select(x => x.Email).ToList();
            if (!customerEmails.Contains(email))
            {
                return false;
            }
            return true;
        }

        public async Task<bool> IsAdmin(string email)
        {

            if (email is null)
            {
                return false;
            }
            Customer adminLogin = await GetDefaultCustomer();
            string admin = adminLogin.Email;
            if (email != admin)
            {
                return false;
            }
            return true;
        }
        public async Task<bool> IsCustomer(string email)
        {

            if (email is null)
            {
                return false;
            }
            var customer = GetByEmail(email);
            if (customer is null)
            {
                return false;
            }
            return true;
        }
    }
}

