﻿using BusinessObjects.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.Threading.Tasks;

namespace DataAcess
{
    public class FlowerBouquetDAO
    {
        private readonly FuflowerBouquetManagementContext _dbContext;

        public FlowerBouquetDAO()
        {
            _dbContext = new FuflowerBouquetManagementContext();
        }
        //public async Task<FlowerBouquet> GetById(int id)
        //{
        //    var result = await _dbContext.FlowerBouquets.Include(c => c.Supplier).FirstOrDefaultAsync(c => c.FlowerBouquetId == id);
        //    return result;
        //}

        //public async Task<IEnumerable<FlowerBouquet>> GetListByName(string name)
        //{
        //    var result = await _dbContext.FlowerBouquets.Where(c => c.FlowerBouquetName.ToLower().Contains(name)).ToListAsync();
        //    return result;
        //}
        public async Task<IEnumerable<FlowerBouquet>> GetList(bool order = false)
        {
            IEnumerable<FlowerBouquet> flowerBouquets = null;
            try
            {
                //if order is true
                if (order)
                {
                    // Get Units In Stock > 0
                    flowerBouquets = await _dbContext.FlowerBouquets
                            .Where(x => x.UnitsInStock > 0)
                           .Include(x => x.Category).Include(x => x.Supplier).ToListAsync();
                }
                else
                {
                    //if order is false
                    flowerBouquets = await _dbContext.FlowerBouquets
                            .Include(x => x.Category).Include(x => x.Supplier).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return flowerBouquets;
        }
        public async Task<FlowerBouquet> GetFlowerBouquetById(int flowerBouquetId, IEnumerable<FlowerBouquet> searchList = null)
        {
            FlowerBouquet flowerBouquet = null;

            try
            {
                if (searchList is null)
                {
                    var query = _dbContext.FlowerBouquets.Where(x => x.FlowerBouquetId == flowerBouquetId)
                        .Include(pro => pro.Category).Include(x=>x.Supplier);
                    if (query is not null && await query.AnyAsync())
                    {
                        flowerBouquet = await query.FirstAsync();
                    }
                }
                else
                {
                    flowerBouquet = await searchList.Where(x => x.FlowerBouquetId == flowerBouquetId)
                        .AsQueryable()
                        .Include(x => x.Category).Include(x=>x.Supplier).FirstAsync();
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return flowerBouquet;
        }
        public async Task<FlowerBouquet> GetFlowerBouquetByName(string flowerBouquetName, IEnumerable<FlowerBouquet> searchList = null)
        {
            FlowerBouquet flowerBouquet = null;

            try
            {
                if (searchList is null)
                {
                    var query = _dbContext.FlowerBouquets.Where(x => x.FlowerBouquetName == flowerBouquetName)
                        .Include(pro => pro.Category);
                    if (query is not null && await query.AnyAsync())
                    {
                        flowerBouquet = await query.FirstAsync();
                    }
                }
                else
                {
                    flowerBouquet = await searchList.Where(x => x.FlowerBouquetName == flowerBouquetName)
                        .AsQueryable()
                        .Include(x => x.Category).FirstAsync();
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return flowerBouquet;
        }
        //public async Task<IEnumerable<FlowerBouquet>> GetAll()
        //{
        //    var result = await _dbContext.FlowerBouquets.Include(c => c.Supplier).Include(x => x.Category).ToListAsync();
        //    return result;
        //}

        public async Task Add(FlowerBouquet flowerBouquet)
        {
            if (flowerBouquet is null)
            {
                throw new Exception("FlowerBouquet is undefined!!");
            }
            try
            {
                if (await GetFlowerBouquetById(flowerBouquet.FlowerBouquetId) is not null)
                {
                    throw new Exception("FlowerBouquet is existed!!Please try again!");
                }
                await _dbContext.FlowerBouquets.AddAsync(flowerBouquet);
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task Update(FlowerBouquet flowerBouquet)
        {
            if (flowerBouquet is null)
            {
                throw new Exception("FlowerBouquet is undefined!!");
            }
            try
            {
                var _flowerBouquet = await GetFlowerBouquetById(flowerBouquet.FlowerBouquetId);
                if (_flowerBouquet is not null)
                {
                    _dbContext.Entry(_flowerBouquet).State = EntityState.Detached; // Detach the existing entity
                    _dbContext.Entry(flowerBouquet).State = EntityState.Modified; // Attach and mark as modified
                    //_dbContext.FlowerBouquets.Update(flowerBouquet);
                    await _dbContext.SaveChangesAsync();
                }
                else
                {
                    throw new Exception("FlowerBouquet does not exist!!");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task Delete(int id)
        {
            try
            {
                FlowerBouquet flowerBouquet = await GetFlowerBouquetById(id);
                if (flowerBouquet is not null)
                {
                    _dbContext.FlowerBouquets.Remove(flowerBouquet);
                    await _dbContext.SaveChangesAsync();
                }
                else
                {
                    throw new Exception("FlowerBouquet does not exist!!");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<int> GetNextId()
        {
            int nextId = -1;
            try
            {
                nextId = await _dbContext.FlowerBouquets.MaxAsync(x => x.FlowerBouquetId) + 1;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return nextId;
        }
        public async Task<IEnumerable<FlowerBouquet>> SearchFlowerBouquet(string name, IEnumerable<FlowerBouquet> searchList = null)
        {
            IEnumerable<FlowerBouquet> searchResult = null;
            try
            {
                if (searchList is null)
                {
                    searchResult = await _dbContext.FlowerBouquets
                                        .Where(x => x.FlowerBouquetName.ToLower().Contains(name.ToLower()))
                                        .Include(x => x.Category).ToListAsync();
                }
                else
                {
                    searchResult = searchList.Where(pro => pro.FlowerBouquetName.ToLower().Contains(name.ToLower())).ToList();
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return searchResult;
        }
        public async Task<IEnumerable<FlowerBouquet>> SearchFlowerBouquet(int startUnit, int endUnit, IEnumerable<FlowerBouquet> searchList = null)
        {
            IEnumerable<FlowerBouquet> searchResult = null;

            try
            {
                if (searchList is null)
                {
                    searchResult = await _dbContext.FlowerBouquets
                                    .Where(x => x.UnitsInStock >= startUnit && x.UnitsInStock <= endUnit)
                                    .Include(x => x.Category).ToListAsync();
                }
                else
                {
                    searchResult = searchList.Where(x => x.UnitsInStock >= startUnit && x.UnitsInStock <= endUnit).ToList();
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return searchResult;
        }

        public async Task<IEnumerable<FlowerBouquet>> SearchFlowerBouquet(decimal startPrice, decimal endPrice, IEnumerable<FlowerBouquet> searchList = null)
        {
            IEnumerable<FlowerBouquet> searchResult = null;

            try
            {
                if (searchList is null)
                {
                    searchResult = await _dbContext.FlowerBouquets
                                        .Where(x => x.UnitPrice >= startPrice && x.UnitPrice <= endPrice)
                                        .Include(x => x.Category).ToListAsync();
                }
                else
                {
                    searchResult = searchList.Where(pro => pro.UnitPrice >= startPrice && pro.UnitPrice <= endPrice).ToList();
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return searchResult;
        }
    }
}
